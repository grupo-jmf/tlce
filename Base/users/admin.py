from django.contrib import admin
from .models import User , Pais , Region , Comuna
# Register your models here.
admin.site.register(User)
admin.site.register(Pais)
admin.site.register(Region)
admin.site.register(Comuna)