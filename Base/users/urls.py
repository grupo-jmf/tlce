from . import views
from django.urls import path

urlpatterns = [
        path('registrarUsuario/', views.registrarUsuario),
        path('iniciarSesion/', views.iniciarSesion),
        path('cambiarDatosUsuario/', views.cambiarDatosUsuario),
        path('cambiarContraseña/', views.cambiarContraseña),
    ]    