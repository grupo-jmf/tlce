from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from .managers import UserManager
from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from django.contrib import messages
from datetime import datetime
from django.core.exceptions import ValidationError
from .models import   Region, Comuna , Pais


def registrarUsuario(request):
    if request.method == 'POST':
        rut = request.POST.get('txtRut')
        nombre = request.POST.get('txtNombre')
        apellido = request.POST.get('txtApellido')
        celular = request.POST.get('numCelular')
        email = request.POST.get('txtCorreo')
        fecha_nac = request.POST.get('txtFechaNac')
        password = request.POST.get('txtPassword')
        region_id = request.POST.get('region')  # ID de la nueva región
        comuna_id = request.POST.get('comuna')  # ID de la nueva comuna
        
        # Obtener el modelo de usuario personalizado
        User = get_user_model()

        try:
            region = Region.objects.get(id=region_id)
            comuna = Comuna.objects.get(id=comuna_id)

            # Crear un nuevo objeto de usuario sin guardar en la base de datos todavía
            nuevo_usuario = User(
                rut=rut,
                name=nombre,
                lastname=apellido,
                celular=celular,
                email=email,
                date_nac=fecha_nac,
                region=region,
                comuna=comuna
            )
            
            # Establecer la contraseña utilizando set_password
            nuevo_usuario.set_password(password)
            
            # Limpiar y validar el usuario
            nuevo_usuario.full_clean()
            
            # Guardar el usuario
            nuevo_usuario.save()
            
            # Redirigir al usuario a la página que desees después del registro
            return redirect('/')  # Por ejemplo, redirigir a la página de inicio

        except Region.DoesNotExist:
            messages.error(request, "La región seleccionada no existe.")
        except Comuna.DoesNotExist:
            messages.error(request, "La comuna seleccionada no existe.")
        except ValidationError as e:
            # Capturar los errores de validación y pasarlos al contexto
            for field, errors in e.message_dict.items():
                for error in errors:
                    if field == 'rut':
                        messages.error(request, error)
                    elif field == '__all__':
                        messages.error(request, error)
                    else:
                        messages.error(request, f"{field.capitalize()}: {error}")

    # Renderizar el formulario de registro con los posibles errores
    regiones = Region.objects.all()
    comunas = Comuna.objects.all()
    return render(request, 'registro.html', {'regiones': regiones, 'comunas': comunas})


def iniciarSesion(request):
    if request.method == 'POST':
        email = request.POST.get('txtCorreo')
        password = request.POST.get('txtPassword')

        # Autenticar al usuario utilizando las credenciales proporcionadas
        user = authenticate(request, email=email, password=password)
        
        # Verificar si el usuario fue autenticado correctamente
        if user is not None:
            # Si el usuario es válido, iniciar sesión y redirigir a la página de inicio
            login(request, user)
            return redirect('/')  # Redirigir a la página de inicio
        else:
            # Si las credenciales son inválidas, mostrar un mensaje de error y renderizar el formulario de inicio de sesión nuevamente
            return render(request, 'login.html', {'error_message': 'Credenciales inválidas. Por favor, inténtelo de nuevo.'})

    # Si el método de solicitud no es POST, renderizará el formulario de inicio de sesión.
    return render(request, 'login.html')

def cambiarDatosUsuario(request):
    if request.method == 'POST':
        # Obtener el usuario actualmente autenticado
        usuario = request.user

        # Obtener la contraseña actual ingresada por el usuario en el formulario
        contraseña_actual = request.POST.get('txtPasswordactual')

        # Autenticar al usuario con la contraseña actual ingresada
        usuario_autenticado = authenticate(email=usuario.email, password=contraseña_actual)

        if usuario_autenticado is not None:
            # Verificación exitosa de la contraseña actual

            # Obtener los nuevos datos del formulario
            nuevo_rut = request.POST.get('txtRut')
            nuevo_nombre = request.POST.get('txtNombre')
            nuevo_apellido = request.POST.get('txtApellido')
            nuevo_celular = request.POST.get('numCelular')
            nuevo_email = request.POST.get('txtCorreo')
            nueva_fecha_nac = request.POST.get('txtFechaNac')
            nueva_contraseña = request.POST.get('txtPassword')
            nueva_region_id = request.POST.get('region')  # ID de la nueva región
            nueva_comuna_id = request.POST.get('comuna')  # ID de la nueva comuna
            
            # Validar si el nuevo correo electrónico ya está registrado para otro usuario
            if nuevo_email != usuario.email and get_user_model().objects.filter(email=nuevo_email).exists():
                messages.error(request, 'El correo electrónico ingresado ya está registrado para otro usuario.')
                return render(request, 'edit_perfil.html')

            # Validar si el nuevo RUT ya está registrado para otro usuario
            if nuevo_rut != usuario.rut and get_user_model().objects.filter(rut=nuevo_rut).exists():
                messages.error(request, 'El RUT ingresado ya está registrado para otro usuario.')
                return render(request, 'edit_perfil.html')

            # Validar la edad del usuario
            if nueva_fecha_nac:
                fecha_nac = datetime.strptime(nueva_fecha_nac, '%Y-%m-%d').date()
                hoy = datetime.now().date()
                edad = hoy.year - fecha_nac.year - ((hoy.month, hoy.day) < (fecha_nac.month, fecha_nac.day))
                if edad < 18:
                    messages.error(request, 'Debes tener al menos 18 años para registrarte.')
                    return render(request, 'edit_perfil.html')

            # Validar que la nueva región y comuna existen
            try:
                nueva_region = Region.objects.get(id=nueva_region_id)
                nueva_comuna = Comuna.objects.get(id=nueva_comuna_id)
            except (Region.DoesNotExist, Comuna.DoesNotExist):
                messages.error(request, 'La región o comuna seleccionada no es válida.')
                return render(request, 'edit_perfil.html')

            # Actualizar los datos del usuario
            usuario.rut = nuevo_rut
            usuario.name = nuevo_nombre
            usuario.lastname = nuevo_apellido
            usuario.celular = nuevo_celular
            usuario.email = nuevo_email
            usuario.date_nac = nueva_fecha_nac
            usuario.region = nueva_region
            usuario.comuna = nueva_comuna
            if nueva_contraseña:
                usuario.set_password(nueva_contraseña)

            # Guardar los cambios en la base de datos
            usuario.save()

            # Mostrar un mensaje de éxito
            messages.success(request, 'Tus datos han sido actualizados exitosamente.')

            # Redirigir al usuario a la página de inicio
            return redirect('/')  # Por ejemplo, redirigir a la página de inicio
        else:
            # La contraseña actual no es correcta, mostrar un mensaje de error
            messages.error(request, 'La contraseña actual es incorrecta.')
            return render(request, 'edit_perfil.html')

    # Si el método de solicitud no es POST o si hay algún error en el proceso,
    # renderizará el formulario para cambiar los datos.
    regiones = Region.objects.all()
    comunas = Comuna.objects.all()
    return render(request, 'edit_perfil.html', {'regiones': regiones, 'comunas': comunas})

from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import render, redirect
from django.contrib import messages

def cambiarContraseña(request):
    if request.method == 'POST':
        uidb64 = request.POST.get('uidb64')
        token = request.POST.get('token')
        nueva_contraseña = request.POST.get('txtPassword')

        if not (uidb64 and token and nueva_contraseña):
            messages.error(request, 'Faltan datos necesarios para cambiar la contraseña.')
            return render(request, 'nuevapss.html')

        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            User = get_user_model()
            user = User.objects.get(pk=uid)

            if default_token_generator.check_token(user, token):
                user.set_password(nueva_contraseña)
                user.save()

                messages.success(request, 'Tu contraseña ha sido actualizada exitosamente.')
                return redirect('/')  # Redirigir a la página principal u otra página deseada
            else:
                messages.error(request, 'El enlace de restablecimiento de contraseña no es válido o ha expirado.')
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, UnicodeDecodeError):
            messages.error(request, 'El enlace de restablecimiento de contraseña no es válido o ha expirado.')

        return render(request, 'nuevapss.html')

    return render(request, 'nuevapss.html')