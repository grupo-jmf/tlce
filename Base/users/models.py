import uuid
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from .managers import UserManager

# Definición del modelo Pais
class Pais(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return f"({self.nombre})"

# Definición del modelo Region
class Region(models.Model):
    nombre = models.CharField(max_length=100)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)

    def __str__(self):
        return f"({self.nombre})"

# Definición del modelo Comuna
class Comuna(models.Model):
    nombre = models.CharField(max_length=100)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    def __str__(self):
        return f"({self.nombre})"

# Definición del modelo User
class User(AbstractBaseUser, PermissionsMixin):
    """Data model for custom user in system"""
    rut = models.CharField(
        _("RUT"), max_length=12, unique=True, null=True, blank=True,
        error_messages={
            'max_length': _("El RUT no puede tener más de 12 caracteres."),
            'unique': _("Este RUT ya está registrado."),
        }
    )
    name = models.CharField(
        _("first name"), max_length=30, blank=False, null=False
    )
    lastname = models.CharField(
        _("last name"), max_length=30, blank=False, null=False
    )
    celular = models.CharField(
        _("celular"), max_length=10, blank=True, null=True
    )
    date_nac = models.DateField(
        _("date of birth"), blank=True, null=True
    )
    email = models.EmailField(
        _("email address"), unique=True,
        error_messages={
            'unique': _("Este correo electrónico ya está registrado."),
        }
    )
    country = models.CharField(
        _("country"),
        max_length=128,
        null=False,
        blank=False,
        default="Chile",
    )
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, blank=True)
    comuna = models.ForeignKey(Comuna, on_delete=models.SET_NULL, null=True, blank=True)
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_(
            "Designates whether the user can log into this admin site."
        ),
    )
    contador_notificaciones = models.IntegerField(default=0)
    is_active = models.BooleanField(_("active"), default=True)
    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name", "lastname"]

    class Meta:
        """Meta definitions for model"""
        verbose_name = _("user")
        verbose_name_plural = _("users")

    @property
    def full_name(self):
        """Returns the full name."""
        full_name = f"{self.name} {self.lastname}"
        return full_name.strip()

    def get_short_name(self):
        """Returns a short name for the user."""
        return self.name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return self.full_name

    def is_adult(self):
        """Returns True if the user is 18 years or older."""
        if self.date_nac:
            today = timezone.now().date()
            age = today.year - self.date_nac.year - ((today.month, today.day) < (self.date_nac.month, self.date_nac.day))
            return age >= 18
        return False

    def clean(self):
        """Custom model validation."""
        from django.core.exceptions import ValidationError

        if not self.is_adult():
            raise ValidationError(_('Debes tener al menos 18 años para registrarte'))

        if self.rut and not validate_rut(self.rut):
            raise ValidationError(_('RUT inválido'))

    def save(self, *args, **kwargs):
        # Ensure full_clean is called to enforce validations
        self.full_clean()
        super().save(*args, **kwargs)

def validate_rut(rut):
    """Validates Chilean RUT"""
    try:
        rut = rut.upper().replace("-", "").replace(".", "").replace(" ", "")
        rut_body = rut[:-1]
        dv = rut[-1]

        reverse_rut_body = rut_body[::-1]
        total = 0
        factor = 2

        for digit in reverse_rut_body:
            total += int(digit) * factor
            factor = factor + 1 if factor < 7 else 2

        remainder = total % 11
        calculated_dv = 11 - remainder
        calculated_dv = "0" if calculated_dv == 11 else "K" if calculated_dv == 10 else str(calculated_dv)

        return calculated_dv == dv
    except:
        return False
