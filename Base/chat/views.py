
# Create your views here.
from django.shortcuts import render


from django.contrib.auth.decorators import login_required
from users.models import User  # Importar el modelo de usuario personalizado

def index(request):
    users = User.objects.all()  # Obtener todos los usuarios
    return render(request, 'chat/index.html', {'users': users})

def room(request, room_name):
    return render(request, "chat/room.html", {"room_name": room_name})