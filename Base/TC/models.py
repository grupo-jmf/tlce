from django.db import models
from django.contrib.auth.hashers import make_password
from users.models import User
import os
from django.conf import settings
from django.core.exceptions import ValidationError
#Define clase categoría con su id y su nombre
class Categoria(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200)
    
    def __str__(self):
        return f"{self.id} ({self.nombre})"
    
 #Define clase condición con su id y el tipo de condición
class Condicion(models.Model):
    id = models.AutoField(primary_key=True)
    condicion = models.CharField(max_length=200)
    
    def __str__(self):
        return f"{self.id} ({self.condicion})"
 
 #Define clase imagen con su id y la dirección de la imagen
class GaleriaImagenes(models.Model):
    id = models.AutoField(primary_key=True)
    imagen = models.ImageField(upload_to='productos', null=True)
    
    def __str__(self):
        return f"{self.id} {self.imagen}"

    def delete(self, *args, **kwargs):
        if self.imagen:
            if os.path.isfile(self.imagen.path):
                os.remove(self.imagen.path)
        super().delete(*args, **kwargs)

 #Define clase producto con todo lo que conlleva 
class Producto(models.Model):

    DISPONIBLE = 'DISP'
    SOLICITADO = 'SOL'
    INTERCAMBIADO = 'INT'
    
    ESTADO_CHOICES = [
        (DISPONIBLE, 'Disponible'),
        (SOLICITADO, 'Solicitado'),
        (INTERCAMBIADO, 'Intercambiado'),
    ]

    id = models.AutoField(primary_key=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    condicion = models.ForeignKey(Condicion, on_delete=models.CASCADE)
    imagen = models.ForeignKey(GaleriaImagenes, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    descripcion = models.CharField(max_length=500)
    fecha = models.DateField()
    estado = models.CharField(
        max_length=4,
        choices=ESTADO_CHOICES,
        default=DISPONIBLE,
    )
    
    def __str__(self):
        return f"{self.nombre} ({self.id})"

    def delete(self, *args, **kwargs):
        self.imagen.delete()
        super().delete(*args, **kwargs)


    #para comentar todo precionar ctrl + k + c 
    #para descomentar preciona ctrl + k + u




class Notificacion(models.Model):
    TIPOS_NOTIFICACION = (
        ('nuevo_producto', 'Nuevo Producto'),
        ('interes_producto', 'Interés en Producto'),
    )

    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    tipo = models.CharField(max_length=20, choices=TIPOS_NOTIFICACION)
    mensaje = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True, blank=True)
    leida = models.BooleanField(default=False)  # Nuevo campo para indicar si la notificación ha sido leída

    def __str__(self):
        return f"{self.usuario} ({self.tipo}) - {self.mensaje}"   

 
class Valoracion(models.Model):
    usuario_evaluado = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='valoraciones_recibidas', on_delete=models.CASCADE)
    usuario_evaluador = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='valoraciones_realizadas', on_delete=models.CASCADE)
    puntuacion = models.IntegerField()
    comentario = models.TextField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.usuario_evaluado.full_name} - Puntuación: {self.puntuacion}"

    def clean(self):
        if self.usuario_evaluado == self.usuario_evaluador:
            raise ValidationError("Un usuario no puede evaluarse a sí mismo.")
        if self.puntuacion < 1 or self.puntuacion > 5:
            raise ValidationError("La puntuación debe estar entre 1 y 5.")

    def save(self, *args, **kwargs):
        self.full_clean()  # Ensure validations are called before saving
        super().save(*args, **kwargs)
    
# class HistorialActividad(models.Model):
#     usuario = models.ForeignKey(User, on_delete=models.CASCADE)
#     actividad = models.CharField(max_length=200)
#     fecha = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         texto = "{0} ({1})"
#         return texto.format(self.usuario)

# class ListaDeseos(models.Model):
#     usuario = models.ForeignKey(User, on_delete=models.CASCADE)
#     nombre_elemento = models.CharField(max_length=100)
#     descripcion = models.TextField(blank=True, null=True)

#     def __str__(self):
#         texto = "{0} ({1})"
#         return texto.format(self.usuario)

