
  document.getElementById('registroForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Evitar que el formulario se envíe automáticamente
    
    var formData = new FormData(this);

    fetch('/registrarUsuario/', {
      method: 'POST',
      body: formData,
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Error al registrar usuario');
      }
      return response.text();
    })
    .then(data => {
      // Verificar si la respuesta contiene un error
      if (data.trim() !== '') {
        // Mostrar un mensaje de alerta más elegante con SweetAlert
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: data.trim(),
          confirmButtonText: 'Ok'
        });
      } else {
        // Si no hay error, redirigir a la página de éxito o hacer cualquier otra acción necesaria
        window.location.href = '/exito/';
      }
    })
    .catch(error => {
      console.error('Error:', error);
      // Mostrar un mensaje de alerta más elegante con SweetAlert
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'El correo ya se encuentre registrado.',
        confirmButtonText: 'Ok'
      });
    });
  });
