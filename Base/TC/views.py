from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic.detail import DetailView
from .models import   Producto, Categoria, Condicion, GaleriaImagenes , Notificacion, Valoracion
from users.models import Region, Comuna , Pais
from django.contrib.auth import logout, get_user_model
from django.utils import timezone
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
#from django.contrib.auth.models import User 
from django.db.models import F
from django.contrib.auth.decorators import login_required
from django.contrib import messages
# Create your views here.
from users.models import User
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.conf import settings
from django.views import View

from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode

def registro(request):
    regiones = Region.objects.all()
    comunas = Comuna.objects.all()
    return render(request, 'registro.html', {'regiones': regiones, 'comunas': comunas})

def nosotros(request):
    return render(request, 'nosotros.html')

@login_required
def pp(request):
    categorias = Categoria.objects.all()
    condiciones = Condicion.objects.all()
    return render(request, 'Post_producto.html', {'categorias': categorias, 'condiciones': condiciones})


def cerrarSesion(request):
    logout(request)
    return redirect('/')

def login(request):
    return render(request, 'login.html')

@login_required
def perfil(request):
    regiones = Region.objects.all()
    comunas = Comuna.objects.all()
    return render(request, 'perfil.html', {'regiones': regiones, 'comunas': comunas})

@login_required
def edperfil(request):
    regiones = Region.objects.all()
    comunas = Comuna.objects.all()
    return render(request, 'edit_perfil.html', {'regiones': regiones, 'comunas': comunas})

@login_required
def publicaciones(request):
    # Filtrar productos por el usuario logueado
    productos_usuario = Producto.objects.filter(usuario=request.user)
    
    # Obtener todos los usuarios
    users = User.objects.all()  # Utiliza el modelo de usuario correcto
    
    # Renderizar la plantilla con los datos
    return render(request, 'mis_publicaciones.html', {'productos': productos_usuario, 'users': users})


def perfil_publico(request, user_id):
    usuario = get_object_or_404(User, id=user_id)
    producto = get_object_or_404(Producto, id=user_id)
    valoraciones = Valoracion.objects.filter(usuario_evaluado=usuario)
    productos_del_usuario = Producto.objects.filter(usuario=usuario)
    return render(request, 'perfilpublico.html', {
        'producto': producto,
        'usuario': usuario,
        'Productos': productos_del_usuario,
        'valoraciones': valoraciones,
        })


def publicacion(request):
    categoria_id = request.GET.get('categoria', None)
    condicion_id = request.GET.get('condicion', None)
    estado = request.GET.get('estado', None)

    query = request.GET.get('q', None)  # Obtén el término de búsqueda del query string

    if categoria_id:
        productos = Producto.objects.filter(categoria_id=categoria_id).exclude(estado=Producto.INTERCAMBIADO)
    elif condicion_id:
        productos = Producto.objects.filter(condicion_id=condicion_id).exclude(estado=Producto.INTERCAMBIADO)
    elif estado:
        productos = Producto.objects.filter(estado=estado).exclude(estado=Producto.INTERCAMBIADO)
    else:
        productos = Producto.objects.exclude(estado=Producto.INTERCAMBIADO)

    if query:
        productos = productos.filter(nombre__icontains=query)  # Filtra por nombre del producto (ajusta según tus necesidades)

    categorias = Categoria.objects.all()
    condiciones = Condicion.objects.all()
    estados = Producto.ESTADO_CHOICES
    context = {
        'Productos': productos,
        'categorias': categorias,
        'condiciones': condiciones,
        'estados': estados,
        'categoria_seleccionada': categoria_id,
        'condicion_seleccionada': condicion_id,
        'estado_seleccionado': estado,
        'query': query,
    }
    return render(request, 'publicaciones.html', context)

def home(request):
    productos = Producto.objects.exclude(estado=Producto.INTERCAMBIADO)
    return render(request, 'principal.html', {'Productos': productos})

@login_required
def publicar_producto(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        categoria_id = request.POST.get('categoria')
        condicion_id = request.POST.get('condicion')
        imagen_archivo = request.FILES.get('imagen')
        descripcion = request.POST.get('descripcion')

        # buscando en la base de datos categorias y condiciones pre pobladas
        categoria = Categoria.objects.get(id=categoria_id)
        condicion = Condicion.objects.get(id=condicion_id)

        # agregando nueva imagen
        nueva_imagen = GaleriaImagenes.objects.create(imagen=imagen_archivo)
        
        # Crear un nuevo objeto de usuario utilizando el gestor de usuarios personalizado
        nuevo_producto = Producto.objects.create(
            categoria = categoria,
            condicion = condicion,
            imagen = nueva_imagen,
            nombre = nombre,
            usuario = request.user,
            descripcion = descripcion,
            fecha=timezone.now()
        )
        Notificacion.objects.create(
            usuario=request.user,
            mensaje=f'Se ha añadido un nuevo producto: {nuevo_producto.nombre}',
            producto=nuevo_producto
        )
        # Redirigir al usuario a la página que desees después del registro
        return redirect('/')  # Por ejemplo, redirigir a la página de inicio
    
    # Si el método de solicitud no es POST o si hay algún error en el proceso, renderizará el formulario de registro.
    categorias = Categoria.objects.all()
    condiciones = Condicion.objects.all()
    return render(request, 'Post_producto.html', {'categorias': categorias, 'condiciones': condiciones})



def detalle_producto(request, id):
    producto = get_object_or_404(Producto, id=id)
    return render(request, 'detalle_producto.html', {'producto': producto})

def cambiar_estado_producto(request, producto_id):
    producto = get_object_or_404(Producto, id=producto_id)
    if request.method == 'POST':
        nuevo_estado = request.POST.get('nuevo_estado')
        if nuevo_estado in [Producto.DISPONIBLE, Producto.SOLICITADO, Producto.INTERCAMBIADO]:
            producto.estado = nuevo_estado
            messages.success(request, f'Estado de {producto.nombre} cambiado correctamente.')
            producto.save()

            # Obtener el ID del usuario seleccionado para el intercambio desde el formulario
            usuario_intercambio_id = request.POST.get('usuario_intercambio')
            if usuario_intercambio_id:
                usuario_intercambio = get_object_or_404(User, id=usuario_intercambio_id)
                # Crear y guardar la notificación
                mensaje = f'Felicidades por realizar un trueke con {request.user.name}! ¿Deseas dejar algún comentario? ' \
                          f'<a href="../valoracion">Haz clic aquí</a>'
                Notificacion.objects.create(
                    usuario=usuario_intercambio,
                    mensaje=mensaje,
                    producto=producto
                )

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required
def vista_editar_producto(request, id):
    categorias = Categoria.objects.all()
    condiciones = Condicion.objects.all()
    producto = get_object_or_404(Producto, id=id)
    return render(request, 'editar_producto.html', {'producto': producto, 'categorias': categorias, 'condiciones': condiciones})

def editar_producto(request, producto_id):
    # Obtener el producto que se va a editar
    producto = get_object_or_404(Producto, id=producto_id)

    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        categoria_id = request.POST.get('categoria')
        condicion_id = request.POST.get('condicion')
        imagen_archivo = request.FILES.get('imagen')
        descripcion = request.POST.get('descripcion')
        nuevo_estado = request.POST.get('nuevo_estado')

        # Buscar en la base de datos categorías y condiciones pre pobladas
        categoria = Categoria.objects.get(id=categoria_id)
        condicion = Condicion.objects.get(id=condicion_id)

        # Actualizar la imagen si se ha subido una nueva
        if imagen_archivo:
            nueva_imagen = GaleriaImagenes.objects.create(imagen=imagen_archivo)
            producto.imagen = nueva_imagen

        # Actualizar los demás campos del producto
        producto.categoria = categoria
        producto.condicion = condicion
        producto.nombre = nombre
        producto.descripcion = descripcion
        producto.fecha_actualizacion = timezone.now()

        # Cambiar el estado del producto si se ha seleccionado uno nuevo
        if nuevo_estado:
            producto.estado = nuevo_estado

        producto.save()

        # Redirigir al usuario a la página que desees después de la edición
        return redirect('/')  # Por ejemplo, redirigir a la página de inicio
    
#Funciones


def expresar_interes(request, producto_id):
    producto = get_object_or_404(Producto, id=producto_id)

    if request.method == 'POST':
        mensaje_interes = request.POST.get('mensaje_interes', '')

        # Crear una nueva notificación de interés en producto
        Notificacion.objects.create(
            usuario=producto.usuario,
            mensaje=f'El usuario {request.user.name} {request.user.lastname} está interesado en tu producto: {producto.nombre}',
            producto=producto
        )

        # Actualizar el contador de notificaciones para el usuario destinatario
        producto.usuario.contador_notificaciones += 1
        producto.usuario.save()

        # Agregar mensaje de éxito
        messages.success(request, 'Tu Mensaje ha sido enviado correctamente.')

        return redirect('detalle_producto', id=producto.id)

    return render(request, 'detalle_producto.html', {'producto': producto})

@login_required
def ver_notificaciones(request):
    notificaciones = Notificacion.objects.filter(usuario=request.user).order_by('-fecha_creacion')
    return render(request, 'notificaciones.html', {'notificaciones': notificaciones})

def contador_notificaciones(request):
    if request.user.is_authenticated:
        contador = Notificacion.objects.filter(usuario=request.user, leida=False).count()
    else:
        contador = 0
    
    return JsonResponse({'contador': contador})


def eliminar_notificacion(request, notificacion_id):
    if request.method == 'POST':
        notificacion = get_object_or_404(Notificacion, id=notificacion_id)
        notificacion.delete()
        # Devolver una respuesta JSON indicando éxito
        return JsonResponse({'message': 'Notificación eliminada correctamente.'})
    else:
        return JsonResponse({'error': 'No se pudo eliminar la notificación.'}, status=400)
    
def eliminar_todas_notificaciones(request):
    if request.method == 'POST':
        Notificacion.objects.filter(usuario=request.user).delete()
        return JsonResponse({'message': 'Todas las notificaciones han sido eliminadas correctamente.'})
    else:
        return JsonResponse({'error': 'No se pudo eliminar todas las notificaciones.'}, status=400)
    
def eliminar_producto(request, producto_id):
    producto = get_object_or_404(Producto, id=producto_id)
    if request.method == 'POST':
        producto.delete()
        messages.success(request, 'Producto eliminado correctamente')
        return redirect(reverse('publicaciones'))
    return render(request, 'mis_publicaciones.html', {'producto': producto})

@login_required
def valoracion(request):
    usuarios_disponibles = User.objects.exclude(id=request.user.id)  # Excluir al usuario autenticado de la lista
    
    if request.method == 'POST':
        usuario_evaluado_id = request.POST.get('usuario_evaluado_id')
        puntuacion = request.POST.get('puntuacion')
        comentario = request.POST.get('comentario')

        try:
            usuario_evaluado = User.objects.get(id=usuario_evaluado_id)
            usuario_evaluador = request.user
            valoracion = Valoracion(usuario_evaluado=usuario_evaluado, usuario_evaluador=usuario_evaluador,
                                    puntuacion=puntuacion, comentario=comentario)
            valoracion.save()
            messages.success(request, 'Valoración enviada exitosamente.')
            return redirect(reverse('home'))  # Redirige a la página de perfil del usuario evaluado
        except User.DoesNotExist:
            messages.error(request, 'El usuario a evaluar no existe.')
        except ValueError:
            messages.error(request, 'Datos inválidos para la valoración.')

    usuarios = User.objects.all()  # Esto te da todos los usuarios, para otros usos
    context = {
        'usuarios_disponibles': usuarios_disponibles,
        'usuarios': usuarios,  # Añadimos todos los usuarios al contexto
    }
    return render(request, 'valoracion.html', context)


class Send(View):
    def get(self, request):
        return render(request, 'mail/send.html')
    
    def post(self, request):
        email = request.POST.get('email')
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            messages.error(request, "No se encontró ningún usuario con ese correo electrónico.")
            return render(request, 'mail/send.html')

        uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
        token = default_token_generator.make_token(user)
        url_cambio_contraseña = request.build_absolute_uri(reverse('nuevapss', args=[uidb64, token]))
        template = get_template('mail/contraol.html')

        # Se renderiza el template y se envían los parámetros
        content = template.render({
            'email': email,
            'url_cambio_contraseña': url_cambio_contraseña
        })

        # Se crea el correo (título, mensaje, emisor, destinatario)
        msg = EmailMultiAlternatives(
            'Cambio de contraseña',
            "Hola usuario",
            settings.EMAIL_HOST_USER,
            [email]
        )

        msg.attach_alternative(content, 'text/html')
        msg.send()

        messages.success(request, 'Se ha enviado un correo electrónico con las instrucciones para cambiar tu contraseña.')
        return render(request, 'mail/send.html')
    
def passn(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        User = get_user_model()
        user = User.objects.get(pk=uid)

        # Verificar el token aquí
        if default_token_generator.check_token(user, token):
            # Procesar la solicitud de cambio de contraseña
            # Renderizar el formulario de cambio de contraseña
            return render(request, 'nuevapss.html', {'uidb64': uidb64, 'token': token, 'email': user.email})
        else:
            # Token inválido, manejar el error adecuadamente
            return HttpResponse('El enlace de restablecimiento de contraseña no es válido o ha expirado.')
    except (TypeError, ValueError, OverflowError, User.DoesNotExist, UnicodeDecodeError):
        # Manejar las excepciones
        return HttpResponse('El enlace de restablecimiento de contraseña no es válido o ha expirado.')  