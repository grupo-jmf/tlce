from django.contrib import admin
from .models import Producto,Categoria,Condicion,GaleriaImagenes, Valoracion
from django.contrib import admin

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    list_filter=('nombre',)
    list_per_page = 10
    
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Producto)
admin.site.register(Condicion)
admin.site.register(GaleriaImagenes)
admin.site.register(Valoracion)