# Generated by Django 5.0.1 on 2024-06-22 06:53

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TC', '0007_notificacion'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Valoracion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puntuacion', models.IntegerField()),
                ('comentario', models.TextField(blank=True, null=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('usuario_evaluado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='valoraciones_recibidas', to=settings.AUTH_USER_MODEL)),
                ('usuario_evaluador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='valoraciones_realizadas', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
