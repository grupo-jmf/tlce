from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views
from .views import Send
urlpatterns = [
     path('', views.home, name='home' ),
     path('registro/', views.registro, name='registro'),
     path('nosotros/', views.nosotros, name='nosotros'),
     path('producto/', views.pp, name='producto'),
     path('login/', views.login, name='login'),
     path('cerrar-sesion/', views.cerrarSesion, name='cerrar_sesion'),
     path('perfil/', views.perfil, name='perfil'),
     path('edperfil/', views.edperfil, name='edperfil'),
     path('mis_publicaciones/', views.publicaciones, name='publicaciones'),
     path('Post_producto/', views.pp, name='post_producto'),
     path('publicar/', views.publicar_producto, name='publicar_producto'),
     path('publicacion/', views.publicacion, name='publicacion'),
     path('detalle_producto/<int:id>', views.detalle_producto, name='detalle_producto'),
     path('cambiar_estado_producto/<int:producto_id>/', views.cambiar_estado_producto, name='cambiar_estado_producto'),
     path('editar_producto/<int:id>', views.vista_editar_producto, name='vista_editar_producto'),
     path('editar_productos/<int:producto_id>/', views.editar_producto, name="editar_productos"),
     path('perfil_publico/<int:user_id>/', views.perfil_publico, name='perfil_publico'),
     path('expresar_interes/<int:producto_id>/', views.expresar_interes, name='expresar_interes'),
     path('notificaciones/', views.ver_notificaciones, name='ver_notificaciones'),
     path('contador_notificaciones/', views.contador_notificaciones, name='contador_notificaciones'),
     path('eliminar-notificacion/<int:notificacion_id>/', views.eliminar_notificacion, name='eliminar_notificacion'),
     path('eliminar-todas-notificaciones/', views.eliminar_todas_notificaciones, name='eliminar_todas_notificaciones'),  # Nueva ruta para eliminar todas las notificaciones
     path('producto/eliminar/<int:producto_id>/', views.eliminar_producto, name='eliminar_producto'),
     path('valoracion/', views.valoracion, name='valoracion'),
     path('send/', Send.as_view(), name='send_email'),
     path('nuevapss/<uidb64>/<token>/', views.passn, name='nuevapss'),

    ]    

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)